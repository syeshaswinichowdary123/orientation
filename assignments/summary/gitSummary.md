# Sumarry on Git

## What is Git and Why?
 Git is a platform that makes your work easier. It helps you to collaborate your teams and interact with the work. To make yourself familiar with Git Workflow let's go through the Basics.

## Git Basics
 
- **Repository:**
It like a box there all your stuff is stored.This is the place where the coding or work you have done will be. Repo is shortend of Repository.

- **Commit:**
This is one of the important thing that you have to remember if not the efforts you have put on may get wasted that is saving your work.This commit saves your work.

- **Push:**
Pushing is essentially syncing your commits to GitLab.
- **Branch:**
This is similar to the branches of trees.You can create a branch for each set of related changes you make.Here repo is a tree and the set of changes as branch.
- **Merge:**
Merge is nothing but interfacing two branches together.
- **Clone:**
It makes an exact copy of entire online repository on your local machine.
- **Fork:**
Forking is a lot like cloning,it makes a duplicate of an existing repo on your local machine under your own name.


## Git has three main states

- Modified: means that you have changed the file but have not committed it to your
repo yet.

- Staged: means that you have marked a modified file in its current version to go into
your next picture/snapshot.

- Committed: means that the data is safely stored in your local repo in form of
pictures/snapshots.

# How to Install Git

*For Linux, open the terminal and type

```bash
    sudo apt-get install git
```
# Git Workflow

![workflow:](extras/Git.png)

1.Workspace : All the changes you make via Editor(s) (gedit, notepad, vim, nano) is
done in this tree of repository.

2.Staging : All the staged files go into this tree of your repository.

3.Local Repository : All the committed files go to this tree of your repository.

4.Remote Repository : This is the copy of your Local Repository but is stored in some
server on the Internet. All the changes you commit into the Local Repository are
not directly reflected into this tree. You need to push your changes to the Remote
Repository

*You clone the repo
```bash
   $ git clone <link-to-repository> 
```
*Create a new branch
```bash
   $ git checkout master
   $ git checkout -b <your-branch-name>
```
*You modify files in your working tree.

*You selectively stage just those changes you want to be part of your next commit,
 which adds only those changes to the staging area.
```bash
  $ git add .         # To add untracked files ( . adds all files) 
```
*You do a commit, which takes the files as they are in the staging area and stores that
snapshot permanently to your Local Git Repository.
```bash
  $ git commit -sv   # Description about the commit
```
*You do a push, which takes the files as they are in the Local Git Repository and stores
that snapshot permanently to your Remote Git Repository.
```bash
  $ git push origin <branch-name>      # push changes into repository
```




 