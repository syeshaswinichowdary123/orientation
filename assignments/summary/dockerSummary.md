# Summary on Dockers

## What is Docker and why it is used?
Docker is a tool designed to make it easier to create, deploy, and run applications by using containers.When we build large projects managing dependencies become harder in that case docker makes it easier.

## Docker Terminologies

### 1.Docker image:
-A Docker image is a read-only template that contains a set of instructions for creating a container that can run on the Docker platform. It provides a convenient way to package up applications and preconfigured server environments.

### 2.Container:
- A Docker container is a running Docker image.
- From one image you can create multiple containers .

### 3.Docker Hub:
- Docker Hub is like GitHub which is used for docker images and containers.

> Completed with the terminologies and Let's get started with the installation.
## Docker Installation
> (ubuntu 16.04)
install Docker engine
```bash
$ sudo apt-get update
$ sudo apt-get install 
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository 
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
// Check if docker is successfully installed in your system
$ sudo docker run hello-world
```
## Docker basic commands
![Docker](extras/docker1.png)
## Common Operations on Dockers
- ** Download the docker:** docker pull snehabhapkar/trydock
- **Run the docker image with this command:** docker run -ti snehabhapkar/trydock /bin/bash
```bash
root@e0b72ff850f8:/# 
> this is id
```
- **Let copy our code inside docker with this command.**
    -  let's take any python file like my.py
    - Copy file inside the docker container: **docker cp hello.py e0b72ff850f8:/ >(this is the id)**
    - All write a script for installing dependencies - requirements.sh
            ```bash
                apt update
                apt install python3
            ```
    - Copy file inside docker:**docker cp requirements.sh e0b72ff850f8:/**
- ** Install dependencies.**
```bash
docker exec -it e0b72ff850f8 chmod +x requirements.sh
docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh
```
- ** Run the program inside container with this command.**
```bash
docker start e0b72ff850f8
docker exec e0b72ff850f8 python3 hello.py
```
- **Save your copied program inside docker image with docker commit.**
```bash
docker commit e0b72ff850f8 snehabhapkar/trydock
```
- **Push docker image to the dockerhub.**
```bash
docker tag snehabhapkar/trydock username/repo
docker push username/repo
```
    





